// Copyright (C) 2017 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_android_kernel",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

cc_defaults {
    name: "vts_treble_vintf_test_defaults",
    cflags: [
        "-Wall",
        "-Werror",
    ],
    shared_libs: [
        "libbase",
        "libbinder",
        "libcutils",
        "libhidlbase",
        "liblog",
        "libutils",
        "libz",
    ],
    static_libs: [
        "apex_aidl_interface-cpp",
        "libaidlmetadata",
        "libgmock",
        "libhidl-gen-hash",
        "libhidl-gen-utils",
        "libhidlmetadata",
        "libprocpartition",
        "libselinux",
        "libtinyxml2",
        "libvintf",
        "libvts_vintf_test_common",
    ],
    srcs: [
        "VtsTrebleVintfTestBase.cpp",
        "utils.cpp",
        "main.cpp",
        ":libvintf_service_info_aidl",
    ],
    data: [
        ":android.hardware",
        ":android.frameworks",
        ":android.system",
        ":android.hidl",
    ],
}

filegroup {
    name: "libvintf_service_info_aidl",
    srcs: [
        "aidl/android/vintf/ServiceInfo.aidl",
        "aidl/android/vintf/IServiceInfoFetcher.aidl",
    ],
    path: "aidl",
}

// Test vendor image that has the highest target FCM version. This test binary
// has no system XML dependencies.
cc_test {
    name: "vts_treble_vintf_vendor_test",
    //Use test_config for vts suite.
    test_config: "vts_treble_vintf_vendor_test.xml",
    test_suites: [
        "vts",
        "general-tests",
    ],
    defaults: ["vts_treble_vintf_test_defaults"],
    srcs: [
        "DeviceManifestTest.cpp",
        "DeviceMatrixTest.cpp",
        "SingleManifestTest.cpp",
    ],
}

cc_test {
    name: "vts_treble_vintf_trusted_hal_test",
    //Use test_config for vts suite.
    test_config: "vts_treble_vintf_trusted_hal_test.xml",
    test_suites: [
        "vts",
        "general-tests",
    ],
    defaults: ["vts_treble_vintf_test_defaults"],
    srcs: [
        "DeviceManifestTest.cpp",
        "DeviceMatrixTest.cpp",
        "SingleManifestTest.cpp",
    ],
    data: [
        ":trusty_test_vm_elf",
        ":trusty_test_vm_config",
        ":trusty_vm_launcher_sh",
        ":trusty_wait_ready_sh",
        ":trusty-ut-ctrl.system",
    ],
    cflags: [
        "-DTRUSTED_HAL_TEST",
    ],
    enabled: false,
    arch: {
        arm64: {
            enabled: true,
        },
    },
}

// Check for HIDL services on device launching after Android 14.
cc_test {
    name: "vts_treble_no_hidl",

    test_suites: [
        "vts",
        "general-tests",
    ],
    defaults: ["vts_treble_vintf_test_defaults"],
    srcs: [
        "VtsNoHidl.cpp",
    ],
}

// Test a (potentially) compatible vendor image with framework dependency;
// test the current framework.
cc_test {
    name: "vts_treble_vintf_framework_test",
    //Use test_config for vts suite.
    test_config: "vts_treble_vintf_framework_test.xml",
    test_suites: [
        "vts",
        "general-tests",
    ],
    defaults: ["vts_treble_vintf_test_defaults"],
    srcs: [
        "SingleManifestTest.cpp",
        "SystemVendorTest.cpp",
    ],
}

// Tests everything.
// Should not be used by VTS framework, but could be used for test development.
// VTS framework should choose among the other test binaries defined above.
cc_test {
    name: "vts_treble_vintf_test_all",
    defaults: ["vts_treble_vintf_test_defaults"],
    srcs: [
        "DeviceManifestTest.cpp",
        "DeviceMatrixTest.cpp",
        "SingleManifestTest.cpp",
        "SystemVendorTest.cpp",
    ],
}
